stages:
  - lint
  - test
  - review

variables:
    BUNDLE: "com.gitlab.newsflash.Devel.flatpak"

flatpak:
    image: "registry.gitlab.gnome.org/gnome/gnome-runtime-images/rust_bundle:3.36"
    stage: "test"
    variables:
        MANIFEST_PATH: "build-aux/com.gitlab.newsflash.json"
        FLATPAK_MODULE: "newsflash"
        CONFIGURE_ARGS: "-Dprofile=development"
        RUNTIME_REPO: "https://sdk.gnome.org/gnome-nightly.flatpakrepo"
        APP_ID: "com.gitlab.newsflash.Devel"

    script:
        # Prepare the manifest
        - >
          sed -i "s/@FEEDLY_CLIENT_ID@/${FEEDLY_CLIENT_ID}/g" ${MANIFEST_PATH}
        - >
          sed -i "s/@FEEDLY_CLIENT_SECRET@/${FEEDLY_CLIENT_SECRET}/g" ${MANIFEST_PATH}
        - >
          sed -i "s/@PASSWORD_CRYPT_KEY@/${PASSWORD_CRYPT_KEY}/g" ${MANIFEST_PATH}
        - >
          sed -i "s/@MERCURY_PARSER_USER@/${MERCURY_PARSER_USER}/g" ${MANIFEST_PATH}
        - >
          sed -i "s/@MERCURY_PARSER_KEY@/${MERCURY_PARSER_KEY}/g" ${MANIFEST_PATH}
        
        - flatpak-builder --stop-at=${FLATPAK_MODULE} app ${MANIFEST_PATH}
        # Build the flatpak repo
        - flatpak-builder --run app ${MANIFEST_PATH} meson --prefix=/app ${CONFIGURE_ARGS} _build
        - flatpak-builder --run app ${MANIFEST_PATH} ninja -C _build dist
        - flatpak-builder --run app ${MANIFEST_PATH} ninja -C _build install

        # Create a flatpak bundle
        - flatpak-builder --finish-only app ${MANIFEST_PATH}
        - flatpak build-export repo app
        - flatpak build-bundle repo ${BUNDLE} ${APP_ID}

    artifacts:
        paths:
            - $BUNDLE
            - _build/meson-dist/
        expire_in: 5 days

    cache:
        key: "flatpak"
        paths:
          - .flatpak-builder/downloads/
          - .flatpak-builder/git/
          - _build/target/
          - _build/target_test/
          - _build/cargo/

review:
    stage: review
    dependencies:
        - flatpak
    script:
        - echo "Generating flatpak deployment"
    artifacts:
        paths:
            - $BUNDLE
        expire_in: 30 days
    environment:
        name: review/$CI_COMMIT_REF_NAME
        url: https://gitlab.gnome.org/$CI_PROJECT_PATH/-/jobs/$CI_JOB_ID/artifacts/raw/${BUNDLE}
        on_stop: stop_review

stop_review:
    stage: review
    script:
        - echo "Stopping flatpak deployment"
    when: manual
    environment:
        name: review/$CI_COMMIT_REF_NAME
        action: stop

# Configure and run rustfmt
# Exits and builds fails if on bad format
rustfmt:
  image: "rust:slim"
  stage: "lint"
  script:
    # Create blank versions of our configured files
    # so rustfmt does not yell about non-existent files or completely empty files
    - echo -e "" >> src/config.rs
    - rustup component add rustfmt
    - rustc -Vv && cargo -Vv
    - cargo fmt --version
    - cargo fmt --all -- --color=always --check
